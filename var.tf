variable "scalr_aws_access_key" {
  type = string
  }

variable "scalr_aws_secret_key" {
  type = string
  }

variable "instance_count" {
  type = string
  default = "1"
  }

variable "region" {
  type = string
  default = "us-east-1"
   }

variable "instance_type" {
  type = string
  default = "t3.medium"
}

variable "network" {
  type = string
  default = "vpc-596aa03e"
 }

variable "subnet" {
  type = string
default = "subnet-7e3fd71a"
  }

variable "associate_public_ip" {
  type    = bool
  default = true
}

variable "tags" {
  type = map
  default = {
    us-east-1 = "image-1234"
    us-west-2 = "image-4567"
  }
}

#variable "hide" {
 # type = string
  #}
